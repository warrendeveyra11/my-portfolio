import React, { useState } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from '@material-ui/core/IconButton';  
import MoreIcon from '@material-ui/icons/MoreVert'
import { Container, Grid, Link } from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import { useStyles } from "./../styles/theme";

import Box from '@material-ui/core/Box';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';

import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { NavigationLogo } from "./svg";

export default function NavigationHeader() {
  const classes = useStyles();
  const theme = useTheme();
  const smallScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [state, setState] = useState({
    top: false
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ 
          paddingTop: "4vh",
          backgroundColor: "#F8F8FF"
      }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <Typography style={{marginBottom: "2vh", textAlign: "center"}} noWrap className={classes.title}>
              <NavigationLogo className={classes.logo}/>
      </Typography>
      <List>
        {['Home', 'About', 'Experience', 'Works', 'Achievements'].map((text, index) => (
          <ListItem key={text} disablePadding>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        {['GitHub', 'GitLab', 'Linked In'].map((text, index) => (
          <ListItem key={text} disablePadding>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <>
      <CssBaseline />
      <AppBar position="sticky" color="default" elevation={0}>
        <Container maxWidth="lg">
          <Toolbar style={{ height: "9vh" }} className={classes.animateTransition}>
            <Typography noWrap className={classes.title}>
              <NavigationLogo className={classes.logo}/>
            </Typography>
            <Grid className={classes.navBarMd}>
              <Link
                color="inherit"
                className={classes.buttonLink}
                component="button"
              >
                <b>Home</b>
              </Link>
              <Link
                color="inherit"
                className={classes.buttonLink}
                component="button"
              >
                <b>About</b>
              </Link>
              <Link
                color="inherit"
                className={classes.buttonLink}
                component="button"
              >
                <b>Experience</b>
              </Link>
              <Link
                color="inherit"
                className={classes.buttonLink}
                component="button"
              >
                <b>Works</b>
              </Link>
              <Link
                color="inherit"
                className={classes.buttonLink}
                component="button"
              >
                <b>Achievements</b>
              </Link>
            </Grid>
            <Grid className={classes.navBarSm}>
            <IconButton
              aria-label="show more"
              // aria-controls={}
              // aria-haspopup="true"
              // onClick={}
              color="inherit"
              onClick={toggleDrawer('top', state.top ? false : true)}
            >
              <MoreIcon />
            </IconButton>
          </Grid>
          </Toolbar>
          <SwipeableDrawer
            style={{ zIndex: "9999"}}
            anchor={'top'}
            open={!smallScreen ? false : state['top']}
            disableSwipeToOpen={true}
            onClose={toggleDrawer('top', false)}
            onOpen={toggleDrawer('top', true)}
          >
              {list('top')}
          </SwipeableDrawer>
        </Container>
      </AppBar>
    </>
  );
}
